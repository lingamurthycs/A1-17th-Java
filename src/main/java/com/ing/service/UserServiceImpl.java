package com.ing.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ing.dto.entity.User;
import com.ing.request.LocateMeResponse;
import com.ing.request.UserDto;

@Service
public class UserServiceImpl implements UserService {

	Map<String, List<UserDto>> message = new HashMap<>();

	@Autowired 
	OfferService offerService;
	
	public LocateMeResponse locateMe(UserDto object) {
		LocateMeResponse resp = new LocateMeResponse();
		if (null != object) {
			Long customerId = object.getCif();
			Long geoFencId = object.getGeoFencId();
			StringBuffer key = new StringBuffer(customerId.toString());
			key.append(geoFencId);
			List<UserDto> users = new ArrayList<>();
			if (message.containsKey(key.toString())) {
				List<UserDto> userObjs = message.get(key.toString());
				Boolean timeFlag = validateTime(object.getTimePlaced(), userObjs);
				if (timeFlag) {
					List<User> userEntity=	offerService.getOfferDetails();
					offerService.saveCustomerDetails(userEntity);
					// Call Google Api
					// clear key in map
					message.remove(key.toString());
					resp.setStausMessage("Success");
				}
				message.get(key.toString()).add(object);

			} else {
				users.add(object);
				message.put(key.toString(), users);
			}

		}
		resp.setStatusCode("Success");
		return resp;
	}

	private Boolean validateTime(Date currentDate, List<UserDto> userObjs) {
		if (null != userObjs && userObjs.size() == 2) {
			for (UserDto userDto : userObjs) {
				Date previousTime = userDto.getTimePlaced();
				long duration = currentDate.getTime() - previousTime.getTime();
				if (duration < 3) {
					return true;
				}
			}

		}
		return false;
	}

}