package com.ing.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ing.dto.OfferDto;
import com.ing.dto.Result;
import com.ing.dto.entity.User;
import com.ing.repository.UserRepository;

@Service
public class OfferService {

	Logger logger = Logger.getLogger(OfferService.class.getName());

	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private UserRepository userRepository;

	public List<User>  getOfferDetails() {

		String URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyDCokM8bvUOU8HcWHKQDVQP8FRF_fXOb0o&name=starbucks&location=-33.87325200,151.20867600&radius=500&type=cafe";

		OfferDto result = restTemplate.getForObject(URL, OfferDto.class);

		logger.debug(result);
		
		List<Result> locationResults = result.getResults();

		List<User> userList = new ArrayList<>();
		for (Result r : locationResults) {
			User user = new User();
			user.setPlaceId(r.getPlaceId());
			user.setPlaceAddress(r.getVicinity());
			user.setExpirationTime(new Date());
			user.setGeneratedTime(new Date());
			user.setState(r.getVicinity());
			user.setCustomerId(1L);
			user.setDeviceId("Test");
			user.setDeviceLocation("Current");
			user.setOfferId(r.getId());
			user.setCreatedDate(new Date());
			user.setTimePlaceId(new Date());
			userList.add(user);
		}

		logger.debug(userList);
		return userList ;

	}

	@Transactional
	public void saveCustomerDetails(List<User> users){
		userRepository.save(users);
	}
	
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
}
