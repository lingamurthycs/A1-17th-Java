package com.ing.service;

import com.ing.request.LocateMeResponse;
import com.ing.request.UserDto;

public interface UserService {

	public LocateMeResponse locateMe(UserDto object);
}
