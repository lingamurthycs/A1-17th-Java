package com.ing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ing.request.LocateMeRequest;
import com.ing.request.LocateMeResponse;
import com.ing.request.UserDto;
import com.ing.service.UserService;

@RestController
@CrossOrigin(origins = "*")
public class UserController {

	@Autowired
	UserService userService;
	

	@RequestMapping(value= "/locateme", method = RequestMethod.POST)
	public LocateMeResponse locateMe(@RequestBody LocateMeRequest request) {
		// Validation
		LocateMeResponse response=new LocateMeResponse();
		StringBuffer resp=validateObject(request);
		if(resp==null || resp.length()>0){
			response.setErrorMessge(resp.toString());
			return response;
		}
		return userService.locateMe(request.getUser());
		
	}
	
	@RequestMapping(value= "/offers", method = RequestMethod.GET)
	public OfferResponse offers() {
		OfferResponse resp=new OfferResponse();
	
		return resp;
		
	}
	
	private StringBuffer validateObject(LocateMeRequest request){
		StringBuffer obj=null;
		if(null!=request){
			obj=new StringBuffer();
			UserDto userObj=request.getUser();
			if(userObj==null){
				obj.append("User Info is null");
			}else{
				if(userObj.getCif()==null ||userObj.getCif()==0){
					obj.append("Customer Id is null");
				}else if(userObj.getDeviceId()==null){
					obj.append("Customer Id is null");
				}else if(userObj.getLatitude()==null || userObj.getLatitude()==0){
					obj.append("Latutude is null");
				}else if(userObj.getLongitude()==null || userObj.getLongitude()==0){
					obj.append("Latutude is null");
				}else if(userObj.getGeoFencId()==null || userObj.getGeoFencId()==0){
					obj.append("Geo Fence Id is null");
				}else if(userObj.getTimePlaced()==null){
					obj.append("Time Placed is null");
				}
			}
		}
		return obj;
	}

	
	
}
