package com.ing.dto.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="customerlocation")
public class User implements Serializable { 
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id" , nullable=false ,insertable=false ,updatable=false)
    private int id;  
	
	@Column(name="cif ")
	private Long customerId;
	
	@Column(name="deviceId")
	private String deviceId;
	
	@Column(name="deviceLocation")
	private String deviceLocation;
	
	@Column(name="offerId")
	private String offerId;
	
	@Column(name="placeId")
	private String  placeId;
	
	@Column(name="placeName")
	private String placeName;
	
	@Column(name="placeAddress")
	private String  placeAddress;
	
	@Column(name="generatedTime")
	private Date generatedTime;
	
	@Column(name="expiryDATE")
	private Date  expirationTime;
	
	@Column(name="state")
	private String state;
	
	@Column(name="createDate")
	private Date  createdDate;
	
	@Column(name="timePlaceId")
	private Date timePlaceId;

	public Date getTimePlaceId() {
		return timePlaceId;
	}

	public void setTimePlaceId(Date timePlaceId) {
		this.timePlaceId = timePlaceId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceLocation() {
		return deviceLocation;
	}

	public void setDeviceLocation(String deviceLocation) {
		this.deviceLocation = deviceLocation;
	}

	public String getOfferId() {
		return offerId;
	}

	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}

	public String getPlaceId() {
		return placeId;
	}

	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}

	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	public String getPlaceAddress() {
		return placeAddress;
	}

	public void setPlaceAddress(String placeAddress) {
		this.placeAddress = placeAddress;
	}

	public Date getGeneratedTime() {
		return generatedTime;
	}

	public void setGeneratedTime(Date generatedTime) {
		this.generatedTime = generatedTime;
	}

	public Date getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(Date expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	

	
	
}  
