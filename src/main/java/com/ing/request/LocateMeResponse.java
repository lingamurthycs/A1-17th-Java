package com.ing.request;

import java.util.Date;

public class LocateMeResponse {
	private String statusCode;
	private String errorCode;
	private String errorMessge;
	private String stausMessage;
	private Date timeForMessage;
	public Date getTimeForMessage() {
		return timeForMessage;
	}
	public void setTimeForMessage(Date timeForMessage) {
		this.timeForMessage = timeForMessage;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessge() {
		return errorMessge;
	}
	public void setErrorMessge(String errorMessge) {
		this.errorMessge = errorMessge;
	}
	public String getStausMessage() {
		return stausMessage;
	}
	public void setStausMessage(String stausMessage) {
		this.stausMessage = stausMessage;
	}

}
