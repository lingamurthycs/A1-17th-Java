package com.ing.request;

import java.io.Serializable;
import java.util.Date;

public class UserDto implements Serializable{
	
	private Long id;
	private Long cif;
	private String deviceId;
	private Double latitude;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCif() {
		return cif;
	}
	public void setCif(Long cif) {
		this.cif = cif;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Long getGeoFencId() {
		return geoFencId;
	}
	public void setGeoFencId(Long geoFencId) {
		this.geoFencId = geoFencId;
	}
	public Date getTimePlaced() {
		return timePlaced;
	}
	public void setTimePlaced(Date timePlaced) {
		this.timePlaced = timePlaced;
	}
	private Double longitude;
	private Long geoFencId;
	private Date timePlaced;
	
	
	
	

}
