package com.ing.offer;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ing.request.LocateMeResponse;
import com.ing.request.UserDto;
import com.ing.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTest {
	    
	@Autowired
    private UserService service;
    
	@Test
	public void testLocateMeWithAllMandatoryParams() {
		UserDto dto=new UserDto();
		dto.setCif(123L);
		dto.setGeoFencId(123L);
		dto.setLatitude(345.23);
		dto.setLongitude(345.23);
		dto.setDeviceId("device");
		dto.setTimePlaced(new Date());
		//given(this.remoteService.someCall()).willReturn("mock");
		LocateMeResponse response = service.locateMe(dto);
		Assert.assertNotNull(response);
	}
	
}
